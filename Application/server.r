#####################################################################
# Patient Referrals Application: Server                            ##
# Author: James Todd                                               ##
# Date: December 2016                                              ##
#####################################################################



###########################################################
# Set up Functions, Libraries & Global Variables         ##
###########################################################

##Load various functions for donwloading list of health conditions, extracting text from pdfs, and classification
source(paste0(getwd(),"/HelperFuncs/HospAppFuncs.R"))                          



##Load needed packages
library(shiny)                                                                 #Required for application
library(shinydashboard)                                                        #Required for application
library(XML)                                                                   #Tools for parsing and generating XML
library(httr)                                                                  #Tools for working with URLs and HTTP
library(stringr)                                                               #Useful functions for manipulating strings
library(pdftools)                                                              #Functions for extracting text from PDFs
library(DT)                                                                    #Allows for better tables



##Define file paths
pdf.dir <<- "./ReferralData/NeuroRefs/"                     #Location of all pdf files

##Central URL for scraping files
ClinicURL <- "https://cpc.health.qld.gov.au/"




##Define starting and ending strings to use in function extracting important text from referrals
Endings <- c("If not [cC]ategory 1",
              "Referral has been accepted by",
              "If not Camgory 1")
Endings <- paste(Endings, collapse = "|")

Starters <- c("Referral:",
               "for Referral",
               "Ref9rral:",
               "Clinical Detalls",
               "for Refenal:")
Starters <- paste(Starters, collapse = "|")





###########################################################
# The Server                                             ##
###########################################################


shinyServer(function(input,output) {
  
  
  
  
  ###########################################################
  # Clinical Prioritisation Criteria                       ##
  ###########################################################
  
  ConditionList <- eventReactive(input$GetConditions, {
    
    ScrapeHealthCons(SiteAddress = ClinicURL)
    
    
  })
  
  
  output$CPC.Table <- DT::renderDataTable(datatable(ConditionList()),
                                      options = list(lengthMenu = c(5,10,25,50),
                                                     pageLength = 10
                                                    )
                                      )

  
  
  
  
  ###########################################################
  # Patient Referrals                                      ##
  ###########################################################
  
  
  GenReferrals <- eventReactive(input$LoadRefs, {
    
    Referral.DF <- LoadRefs(fileLoc = pdf.dir)
    
    #Initialise column for important text
    Referral.DF$ImpText <- rep("String", times = dim(Referral.DF)[1])
    
    #Loop through each record in the data frame and fill in the important text
    for(i in 1:dim(Referral.DF)[1]) {
      Referral.DF$ImpText[i] <- ExtractText(fileLoc = paste0(pdf.dir, list.files(pdf.dir)[i]),
                                            Starters = Starters, Enders = Enders)
    }
    
    
    #Need to initialise this column before attempting to match the referrals with conditions
    Referral.DF$Matches <- "Placeholder"
    
    ##Loop matching the conditions with referrals 
    ####Uses the keywords from conditions and all text from referrals
    for(i in 1:length(Referral.DF$Text)){
      
      Matches <- sapply(X = ConditionList()$KeyWords, FUN = ConditionMatch,
                        Referral = Referral.DF$Text[i])
      names(Matches) <- ConditionList()$Condition
      Matches <- sort(Matches, decreasing = TRUE)
      
      if(sum(Matches > 0) == 0) {
        Matches <- "No Matches"
      } else {
        
        Matches <- Matches[order(Matches, decreasing = TRUE)[1:3]]
        Matches <- paste(names(Matches), "(", round(Matches,4)*100, "%)", sep = "")
        Matches <- paste(Matches, collapse = ", ")
        
      }
      
      Referral.DF$Matches[i] <- Matches
      
    }
    
    colnames(Referral.DF)[colnames(Referral.DF) == "ImpText"] <- "Important Text"
    return(Referral.DF)
    
    
  })
  
  output$Ref.Table <- DT::renderDataTable(GenReferrals(),
                                      options = list(lengthMenu = c(1,3,5),
                                                     pageLength = 3,
                                                     columnDefs = list(list(width = '30%', targets = list(2,3)),
                                                                       list(targets = 2,
                                                                            render = JS("function(data, type, row, meta) {",
                                                                                        "return type === 'display' && data.length > 500 ?",
                                                                                        "'<span title=\"' + data + '\">' + data.substr(0,500) + '...</span>' : data;",
                                                                                        "}")
                                                                            ))
                                      )
  )
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
})
















