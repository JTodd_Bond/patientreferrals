#####################################################################
# Patient Referrals Application: User Interface                    ##
# Author: James Todd                                               ##
# Date: December 2016                                              ##
#####################################################################


#Libraries
library(shiny)
library(shinydashboard)






###########################################################
# Set Up Side Bar                                        ##
###########################################################

sidebar <- dashboardSidebar(
  
  sidebarMenu(
    menuItem("Clinical Prioritisation Criteria", tabName = "CPC", icon = icon("ambulance")),
    menuItem("Referrals", tabName = "Referrals", icon = icon("table"))
  )
  
)





###########################################################
# Set Up Body                                            ##
###########################################################

body <- dashboardBody(
  
  tabItems(
    tabItem(tabName = "CPC",
            
            fluidRow(
              
              
              box(title = "Clinical Priorisation Criteria", 
                  status = "success",
                  solidHeader = TRUE,
                  DT::dataTableOutput("CPC.Table")),
              
              
              
              
              actionButton(inputId = "GetConditions",label = "Download Condition List")
              
              
              
            )
    ),
    
    tabItem(tabName = "Referrals",
            
            
            box(title = "Patient Referrals", 
                status = "success",
                solidHeader = TRUE,
                DT::dataTableOutput("Ref.Table"),
                width = 12),
            
            actionButton(inputId = "LoadRefs", label = "Extract Text From Referrals")
            
            )
  )
              
  
  
)




###########################################################
# Putting it all together                                ##
###########################################################


dashboardPage(
  dashboardHeader(title = "Patient Referrals"),
  sidebar,
  body
)










