####################################################################################
# Referral Application Functions                                                  ##
# Author: James Todd                                                              ##
# Date: December 2016                                                             ##
# Functions used within the Referral Application                                  ##
####################################################################################















##########################################
# Scrape List of Health Conditions      ##
##########################################


ScrapeHealthCons <- function(SiteAddress) {
  
  #Access website
  SiteURL <- GET(SiteAddress)
  WebSite <- htmlTreeParse(SiteURL, error = function(...){}, 
                           useInternalNodes = TRUE,encoding = "UTF-8",
                           trim = TRUE)
  
  #Extract conditions
  Conditions <- getNodeSet(WebSite, "//*/div[@class='col-md-12']")
  Adult.Con <- xpathSApply(Conditions[[3]], ".//a[@class='all abc-list adult']", xmlValue)
  Child.Con <- xpathSApply(Conditions[[3]], ".//a[@class='all abc-list paediatric']", xmlValue)
  
  #Place conditions in data frame, with Adult / Paediatric tag
  ConditionDF <- data.frame(Condition = c(Adult.Con,Child.Con),
                            Tag = c(rep("Adult", times = length(Adult.Con)),
                                    rep("Paediatric", times = length(Child.Con))))
  
  #Extract keywords from the condition elements
  ConditionDF$KeyWords <- gsub(",|/|\\(|\\)| [Nn]on | with | of | to "," ",ConditionDF$Condition)
  ConditionDF$KeyWords <- gsub(" and ", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" or ", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub("[^[:alnum:]]", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" s ", "s ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" to ", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub("specialist|referral", "", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" for ", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" other ", " ", ConditionDF$KeyWords)
  ConditionDF$KeyWords <- gsub(" +", " ", ConditionDF$KeyWords)
  
  
  #Return the final data frame
  return(ConditionDF)
  
}









##########################################
# Read Referrals - All Text             ##
##########################################



LoadRefs <- function(fileLoc) {
  
  #Construct data frame with referral file names
  Referral.DF <- data.frame(Files = list.files(fileLoc),
                            Text = as.character(1:length(list.files(fileLoc))),
                            stringsAsFactors = FALSE)
  
  #Loop through each referral file
  for(i in 1:(length(list.files(pdf.dir)))) {
    
    
    CleanPDF <- pdf_text(paste0(pdf.dir,list.files(pdf.dir)[i]))               #Read in referral file
    CleanPDF <- gsub(" +", " ", CleanPDF)                                      #Remove excess white space
    CleanPDF <- gsub("\r|\n", "", CleanPDF)                                    #Remove some non-words
    
    Referral.DF[i,"Text"] <- CleanPDF                                          #Assign to the original data frame
    
  }
  
  Referral.DF$Text <- gsub("[[:punct:]]", " ", Referral.DF$Text)               #Remove punctuation
  Referral.DF$Text <- gsub(" +", " ", Referral.DF$Text)                        #Remove new excess white space
  Referral.DF$Text <- tolower(Referral.DF$Text)                                #Convert all text to lower case
  
  return(Referral.DF)
  
}








##########################################
# Read Referrals - Important Text       ##
##########################################


# Referral.DF$ImpText <- rep("String", times = dim(Referral.DF)[1])
###Function to extract important part of text in PDF if possible. Also does some further clean up
ExtractText <- function(fileLoc, Starters, Enders) {
  
  ##Extract text from pdf
  Referral <- pdf_text(fileLoc)                                                  #Read file
  Referral <- c(do.call("cbind",str_split(string = Referral, "\n")))             #Split different lines into separate strings
  
  ##Subset text
  Start <- grep(Starters, Referral)[1]                                           #Locate start of important section
  if(is.na(Start[1])) Start <- 1                                                 #If cannot find start, go from beginning
  
  End <- grep(Endings, Referral)[1] - 1                                          #Locate end of important section
  if(is.na(End[1])) End <- length(Referral)                                      #If cannot find end, use end of document
  
  Referral <- Referral[Start:End]                                                #Subset strings to those between start and end
  
  
  ##Clean up text
  Referral <- gsub(".*:","",Referral)                                            #Remove "Reason for Referral:", however text is read in
  Referral <- gsub("\r", "", Referral)                                           #Remove (html?) characters 
  
  Referral <- paste(Referral, collapse = " ")                                    #Put all the strings back into single string again
  Referral <- gsub(" +", " ", Referral)                                          #Remove odd spacing 
  
  Encoding(Referral) <- "UTF-8"                                                  #Ensure all characters from savable encoding
  Referral <- iconv(Referral, "UTF-8", "UTF-8", sub = "")
  
  Referral <- gsub("[[:punct:]]", "", Referral)                                  #Remove punctuation
  Referral <- str_trim(Referral, side = "both")                                  #Remove trailing white space
  
  #Return the identified and cleaned up text
  return(Referral)
  
}










##########################################
# Match up condition list with referrals##
##########################################

##Define function counting number of matches between referral and condition list
ConditionMatch <- function(Conditions, Referral) {
  
  RefWords <- str_split(string = Referral, pattern = " ")[[1]]
  ConWords <- str_split(string = Conditions, pattern = " ")[[1]]
  ConWords <- ConWords[nchar(ConWords) > 1]
  Matches <- sum(RefWords %in% ConWords) / length(ConWords)
  return(Matches)
  
}







